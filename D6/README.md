**FAIR LICENSE**

Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de

Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.


# D6 on NXT / HT

Implements a D6 on a NXT and HT Prototype board. The actual D6 is implemented
using circuitry. The NXT program is written in NXC. It generates a random
number in the range [1,6], prints it on the Screen and sends it to the
HT board.


## Content

* D6.nxc 				the NXC program
* D6.cir 				the D6 circuitry in JavaBreadBoardSimulator format
* D6.3gp 				demo video


## Part List & Requirements

- 1 NXT
- 1 Touch Sensor
- 2 Sensor / Actuator wires
- 1 HiTechnics NXT Prototyping Sensor Kit
- 7 LEDs (any color) 5mm
- 7 resistors 220r (red,red,brown,gold)
  (actually less then 7 resistors are needed when clever used)
- 1 LS 7408 (AND gate)
- 1 LS 7432 (OR gate)

Furthermore the NXC / NBC compiler is needed to actually compile the sources.
See links.


## Building Instruction HT board

A D6 has 7 possible pips indicating the number. These are represented
by 7 LEDs (labeled A to G):

A   B  
C D E              or       C B A D G F E  
F   G  
(as a die)                  (as an array)  

Since it's slightly more manageable to implement the circuit as an array the
building instruction assumes this approach. The circuit implements the
following truth table:

| XYZ | ABCDEFG |
|-----|---------|
| 000 | xxxxxxx |
| 001 | 0001000 |
| 010 | 0000000 |
| 011 | 0001000 |
| 100 | 1100011 |
| 101 | 1101011 |
| 110 | 1110111 |
| 111 | xxxxxxx |

`x` is don't care. The inputs `X`,`Y`,`Z` correspond to the HT prototype board
binary I/Os B0, B1, B2.

Optimized the circuit implements the following functions:

A=G = X + Y  
B=F = X  
C=E = X * Y  
D   = Z  

The circuit itself can be viewed and simulated using the Java Breadboard
Simulator (see links) and the provided file d6.cir. Notes on the circuit:

- Row 'A' represents '+'
- Row 'L' represents '-'
- The DIP switches represent the three inputs B0,B1,B2 (left to right)
- Red wires are 5v power wires, which should be connected either directly
  to the HT boards 5v output or via the breadboards '+' line.
- The black wires represent the 220r resistors
- The circuit simulation lacks the ground wire. It should connect '-' to
  'GND' on the HT board


## Building Instruction NXT

1. Connect the touch sensor to input 1
2. Connect the HT prototype board to input 2

The mapping can be changed in the sources using the appropriated define
directives.


## Compile and Install

Compile

	nbc -Z2 -O=D6.rxe D6.nxc

Install

	nxtcom D6.rxe 
 

## Links

* http://bricxcc.sourceforge.net/nbc/ (NXC / NBC)
* http://www.hitechnic.com/ (hitechnic)
* http://www.cs.york.ac.uk/jbb (java breadboard simulator)

