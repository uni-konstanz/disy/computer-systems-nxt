**FAIR LICENSE**

Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de

Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.


# NXT Line Follower in NXC

Implements a line following algorithm on a NXT using NXC.
It supports output of state to an led array implemented using the HT Prororype board.
This is optional and not required.

## Content

- `followline.nxc`  
    the NXC program

- `BuildingInstructions.lxf`  
    building instructions for [Lego Digital Designer](http://ldd.lego.com)

- `ledarray.cir`  
    circuit simulator for optional led output array

## Requirements

To run the program you need the following tools and parts.

* [NXC](http://bricxcc.sourceforge.net/nbc/) to compile the program

For the LineFollower Model you need

- 1 NXT 
- 1 Light Sensor
- 3 Sensor / Actuator wires
- 2 Actuators

### Optional

To view the building instrunctions (`BuildingInstructions.lxf`) you need the [Lego Digital Designer](http://ldd.lego.com)

To use the optional LED output array to display debug state information, you need

- 1 HiTechnics NXT Prototyping Sensor Kit (out of print, see [HiTechnic](https://www.hitechnic.com/) for alternatives)
- 7 LEDs (any color) 5mm
- 7 resistors 220r (red,red,brown,gold)
  (actually less then 7 resistors are needed when clever used)
- 1 LS 7408 (AND gate)
- 1 LS 7432 (OR gate)

To view the circuit (`ledarray.cir`) you need [Java BreadBoard Simulator version 1.1](https://www.cs.york.ac.uk/jbb/archive/toolset/JBB-V1.zip/view.html)

## Compile and Install

Compile

	nbc -Z2 -O=followline.rxe followline.nxc

Install

	nxtcom followline.rxe 
 

## Links

* [NXC / NBC](http://bricxcc.sourceforge.net/nbc/)
* [Lego Digital Designer](http://ldd.lego.com)
* [HiTechnic](http://www.hitechnic.com/)
* [Java BreadBoard Simulator](http://www.cs.york.ac.uk/jbb)

