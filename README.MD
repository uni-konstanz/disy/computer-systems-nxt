**FAIR LICENSE**

Copyright (c) 2010-2018 thomas.zink _at_ uni-konstanz _dot_ de

Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

# Computer Systems and Networks LEGO NXT Resources

All LEGO NXT and HiTechnics related material used in the Lecture "Computer Systems and Networks".
The lecture used *LEGO Mindstorms Education* sets and *HiTechnics Prototype sensor kit*s as
well as several other components like LEDs and resistors to build several different models.

The NXT models are programmed using [NXC (Not eXactly C) and NBC (Next Byte Codes)](http://bricxcc.sourceforge.net/nbc/), which are C and assembly languages for Lego Mindstorms NXT. They require the unofficial NBC/NXC firmware.

## Contents

- `BuildingInstructions.lxf`: The building instructions for the robot model. Use with [Lego Digital Designer](http://ldd.lego.com)
- `D6`: Implementation of a 6 sided dice (d6) using the NXT brick, the HiTechnics Prototype set, and several LEDs
- `Documents`: Receipts, Lego Manuals, Part List etc
- `HelloWorld`: classic hello world examples for NBC and NXC
- `HTexperiments`: official HiTechnics experiment examples
- `LineFollower`: NXC implementation of a line following algorithm
- `MazeSolver`: right-hand maze solving algorithm in NXC and NBC
- `Playground`: simple tests and examples for NBC, NXC, and HiTechnic

For in-depth documentation see the specific README.MD files in the respective folders.

## Links

- [Lego Education](https://education.lego.com/en-us)
- [Lego Mindstorms NXT Software](https://www.lego.com/en-us/mindstorms/downloads/nxt-software-download)
- [HiTechnic](http://www.hitechnic.com/)
- [Lego Digital Designer](http://ldd.lego.com)
- [Java BreadBoard Simulator](http://www.cs.york.ac.uk/jbb)

