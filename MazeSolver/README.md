**FAIR LICENSE**

Copyright (c) 2010-2017 thomas.zink _at_ uni-konstanz _dot_ de

Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.  
DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.

# NXT Maze Solver

Implements the right hand maze solving algorithm in NXC / NBC. The NXT is
built using a Lego Mindstorms NXT Education set (though a standard retail
set should also do). Mazes are randomly generated using a maze generator
and can be printed out as a 1m^2 poster. Instead of mazes, other material,
like duct tape, can be used to model the mazes.

Mazes must have one of three allowed specific color schemes. See the file 
`/src/world.h` for details on maze characteristics.

## Content

- Makefile: gnu make Makefile  
- BuildingInstructions.lxf: building instructions for [Lego Digital Designer](http://ldd.lego.com)
- src/:	source directory  
	- maze.nxc: main maze solver application  
	- robot.h: all robot related definitions and tasks  
	- world.h: everything related to defining and observing the maze  
	- debug.h: debugging tasks and definitions  
	- libNXC.h: useful library functions in NXC  
	- libNBC.h: same library functions as in libNXC but in NBC  
- tst/: test files  
	- testlib.h: provides very basic library testing  
- mazes/: example mazes  


## Part List & Requirements

- 1 Lego Mindstorms NXT Education set (also works with retail set)
- 1 HT Color Sensor (if Lego Color Sensor is used, world.h needs to be rewritten)
- 1 (optional) HT Prototype Sensor Kit
- 1 Maze


## Building Instructions

### Lego Model

The model used is the same as the LineFollower (see `BuildingInstructions.lxf`).
Alternatively, the standard Lego Mindstorms NXT Education model can be used.
But the sensor array must be slightly altered to allow the HT Color Sensor to be positioned
parallel to the surface (you can figure that out).

### Software 

	make
