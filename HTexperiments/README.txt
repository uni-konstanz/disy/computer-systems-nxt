============================================================================
HT Prototyping Sensor Kit Introduction
============================================================================
These experiments give a short and simple introduction into implementing
circuits using the HiTechnic Prototyping Sensor Kit. 

Please mind the following:

- Model and simulate every circuit using the Java Breadboard Simulator
  prior to implementing it.

- LED receive power on the longer pin (anode +). The shorter (cathode -)
  is connected to ground (this is also the flat side of the LED).

- LEDs need to be connected to a 220 Ohm resistor. Either on + or - side.
  Multiple LEDs on one resistor are possible.

- Chips receive power on VCC and must be grounded on GND. Holding the chip
  such that you can read the text, VCC is top left and GND is bottom right.

-----------------------------------------------------------------------------
Content
-----------------------------------------------------------------------------
.
/README.txt				this readme file
/HTexperiments.nxc		NXC test program for the experiments
/Exp1.cir				JBreadBoard simulator file for E1
/Exp1b.cir				JBreadBoard simulator file for E2a
/Exp2b.cir				JBreadBoard simulator file for E2b
/Exp3.cir				JBreadBoard simulator file for E3

To use the simulator files you need JBreadBoard, a Java based bread board
simulator. It can be used to simulate quite a number of circuits.

-----------------------------------------------------------------------------
Requirements
-----------------------------------------------------------------------------
To run the experiments you will need:

- 1 NXT
- 2 Touch Sensors
- 1 HT Prototyping Sensor Kit
- 3 LEDs (5mm)
- 1 Resistor (220 ohm)
- 1 gate 7408
- 1 gate 7432

-----------------------------------------------------------------------------
Getting started
-----------------------------------------------------------------------------
All experiments use the same NXC program and the same NXT setup.

[1] Connect the touch sensors to NXT input ports 1 and 2.
[2] Connect the HT prototype board to NXT input port 3.
[3] HT prototype board GND -> a61
[4] Resistor e61 <-> -61 (blue, after j, ground)
[5] Wire b49 <-> +42 (red, before a, power)
[6] Compile 2TouchLeds.nxc and transfer it to the NXT.

-----------------------------------------------------------------------------
Experiment 1: Flash on touch
-----------------------------------------------------------------------------
The first experiment simply uses each touch sensor to flash an individual LED.

O0 = X
O1 = Y

[1] LED e55 <-> f55
[2] LED e54 <-> f54
[3] Wire j55 <-> -55
[4] Wire j54 <-> -54

Test using the test program.

-----------------------------------------------------------------------------
Experiment 2a: Flash on both
-----------------------------------------------------------------------------
Flashes a single LED only if both touch sensors are pressed.

O = X * Y

[1] 7408 VCC -> e41 	(AND gate)
[2] Wire +41 <-> a41	(power for gate)
[3] Wire j35 <-> -35 	(ground for gat)

[4] LED e33 <-> f33		(LED)
[5] Wire j33 <-> -33	(ground for led)

[6] Wire e55 <-> j41 	(x)
[7] Wire d54 <-> i40	(y)
[8] Wire g39 <-> d33	(x and y)

Test using the test program

-----------------------------------------------------------------------------
Experiment 2b: Flash on either
-----------------------------------------------------------------------------
The setup is very equal with Experiment 2a. Just replace the gate 7408 with
7432.

O = X + Y

-----------------------------------------------------------------------------
Experiment 3: Show the number
-----------------------------------------------------------------------------
In this experiment the task is to use three LEDs and show the number
pressed using the two touch sensors.

Truth table

X Y | A B C
----+------
0 0 | 0 0 0
0 1 | 1 0 0
1 0 | 1 1 0
1 1 | 1 1 1

Output functions:

A = (!X * Y) + (X * !Y) + (X * Y) = (X ^ Y) + (X * Y) = X + Y

    X !X
 Y  1  1
!Y  1

B = X
C = X * Y

Figure out the wiring yourself.

-----------------------------------------------------------------------------
Where to go ...
-----------------------------------------------------------------------------
There are numerous possibilities to proceed from here.

[1] Try implementing experiment 2 and 3 using only NAND gates (7400)
[2] Coming from here you can incrementally advance to a D6 circuitry or even
an LCD-like circuitry.
